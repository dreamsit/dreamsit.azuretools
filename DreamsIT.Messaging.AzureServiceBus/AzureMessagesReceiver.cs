﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Messaging.Definitions;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;

namespace DreamsIT.Messaging.AzureServiceBus
{
    public class AzureMessagesReceiver<TKey, TType> : IMessagesReceiver<TKey, TType> where TType : class
    {
        private readonly IStringParser<TKey> _parser;
        private readonly QueueClient _client;
        private readonly SubscriptionClient _subscriptionClient;

        public AzureMessagesReceiver(string connectionString, IStringParser<TKey> parser, string queueName = null,
            string topicName = null, string subscriptionName = null, ReceiveMode mode = ReceiveMode.ReceiveAndDelete)
        {
            _parser = parser;
            var namespaceManager = NamespaceManager.CreateFromConnectionString(connectionString);

            if (!String.IsNullOrEmpty(queueName))
            {
                if (!namespaceManager.QueueExists(queueName))
                {
                    namespaceManager.CreateQueue(queueName);
                }
                _client = QueueClient.CreateFromConnectionString(connectionString, queueName, mode);
            }
            else if (!String.IsNullOrEmpty(topicName) && !String.IsNullOrEmpty(subscriptionName))
            {
                TopicDescription topic;
                if (!namespaceManager.TopicExists(topicName))
                {
                    topic = namespaceManager.CreateTopic(topicName);
                }
                else
                {
                    topic = namespaceManager.GetTopic(topicName);
                }

                if (!namespaceManager.SubscriptionExists(topic.Path, subscriptionName))
                {
                    namespaceManager.CreateSubscription(topic.Path, subscriptionName);
                }

                var factory = MessagingFactory.CreateFromConnectionString(connectionString);
                _subscriptionClient = factory.CreateSubscriptionClient(topic.Path, subscriptionName, mode);
            }
            else
            {
                throw new Exception("Queue name or topic and subscription name must be provided");
            }
        }

        public IBrokeredMessage<TKey, TType> Receive(TimeSpan timeout)
        {
            var message = _client != null ? _client.Receive(timeout) : _subscriptionClient.Receive(timeout);
            return new AzureBrokeredMessage<TKey, TType>(message, _parser);
        }

        public async Task<IBrokeredMessage<TKey, TType>> ReceiveAsync(TimeSpan timeout)
        {
            var message = _client != null ? await _client.ReceiveAsync(timeout) : await _subscriptionClient.ReceiveAsync(timeout);
            return new AzureBrokeredMessage<TKey, TType>(message, _parser);
        }

        public void OnMessage(Action<IBrokeredMessage<TKey, TType>> callback)
        {
            if (_client != null)
            {
                _client.OnMessage((msg) => {
                    callback(new AzureBrokeredMessage<TKey, TType>(msg, _parser));
                });
                return;
            }

            _subscriptionClient.OnMessage((msg) => {
                                                       callback(new AzureBrokeredMessage<TKey, TType>(msg, _parser)); });
        }

        public void Dispose()
        {
            if (_client != null)
            {
                _client.Close();
            }
            if (_subscriptionClient != null)
            {
                _subscriptionClient.Close();
            }
        }
    }
}
