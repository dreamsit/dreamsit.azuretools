﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Messaging.Definitions;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;

namespace DreamsIT.Messaging.AzureServiceBus
{
    public class AzureMessageSender<TKey, TType> : IMessagesSender<TKey, TType>, IDisposable where TType : class
    {
        protected readonly string _connectionString;
        private readonly IStringParser<TKey> _keyParser;
        private readonly QueueDescription _description;
        private QueueClient _queueClient;

        public AzureMessageSender(string connectionString, string queueName,IStringParser<TKey> keyParser, QueueDescription description=null)
        {
            _connectionString = connectionString;
            _keyParser = keyParser;
            QueueName = queueName;
            _description = description ?? new QueueDescription(QueueName)
            {
                MaxSizeInMegabytes = 5120,
                DefaultMessageTimeToLive = new TimeSpan(0, 10, 0)
            };
            // Create a new Queue with custom settings
            var namespaceManager = NamespaceManager.CreateFromConnectionString(_connectionString);

            if (!namespaceManager.QueueExists(QueueName))
            {
                namespaceManager.CreateQueue(_description);
            }
            _queueClient = QueueClient.CreateFromConnectionString(_connectionString, QueueName);
        }

        public string QueueName { get; protected set; }

        public IBrokeredMessage<TKey, TType> SendMessage(TType message, TKey messageId = default(TKey), string label = null)
        {
            BrokeredMessage m=new BrokeredMessage(message)
            {
                MessageId =messageId!=null && !messageId.Equals(default(TKey)) ? messageId.ToString() : Guid.NewGuid().ToString(),
                Label = label
            };
            _queueClient.Send(m);
            return new AzureBrokeredMessage<TKey, TType>(m,_keyParser);
        }

        public async Task<IBrokeredMessage<TKey, TType>> SendMessageAsync(TType message, TKey messageId = default(TKey), string label = null)
        {
            BrokeredMessage m = new BrokeredMessage(message)
            {
                MessageId = !messageId.Equals(default(TKey)) ? messageId.ToString() : Guid.NewGuid().ToString(),
                Label = label
            };
            await _queueClient.SendAsync(m);
            return new AzureBrokeredMessage<TKey, TType>(m, _keyParser);
        }

        public void Dispose()
        {
            if (_queueClient != null)
            {
                _queueClient.Close();
            }
        }
    }
}
