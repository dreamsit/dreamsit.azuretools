﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Messaging.Definitions;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;

namespace DreamsIT.Messaging.AzureServiceBus
{
    public class AzureTopicMessageSender<TKey,TType>:IDisposable, ITopicSender<TKey,TType> where TType : class
    {
        private readonly string _connectionString;
        private readonly IStringParser<TKey> _keyParser;
        private readonly TopicDescription _settings;
        private TopicClient _topicClient;

        public AzureTopicMessageSender(string connectionString, string topicName, IStringParser<TKey> keyParser, TopicDescription settings = null)
        {
            _connectionString = connectionString;
            _keyParser = keyParser;
            TopicName = topicName;
            _settings = settings ?? new TopicDescription(TopicName)
            {
                DefaultMessageTimeToLive = TimeSpan.FromMinutes(10),
                EnableExpress = true
            };

            // Create a new Queue with custom settings
            var namespaceManager = NamespaceManager.CreateFromConnectionString(_connectionString);

            if (!namespaceManager.TopicExists(TopicName))
            {
                namespaceManager.CreateTopic(_settings);
            }
            _topicClient = TopicClient.CreateFromConnectionString(_connectionString, TopicName);
        }

        public IBrokeredMessage<TKey, TType> SendMessageToTopic(TType message, TKey messageId = default(TKey), string label = null)
        {
            var m = new BrokeredMessage(message)
            {
                MessageId = messageId!=null && !messageId.Equals(default(TKey)) ? messageId.ToString() : Guid.NewGuid().ToString(),
                Label = label,
                TimeToLive = TimeSpan.FromMinutes(10)
            };

            _topicClient.Send(m);

            return new AzureBrokeredMessage<TKey, TType>(m,_keyParser);
        }

        public async Task<IBrokeredMessage<TKey, TType>> SendMessageToTopicAsync(TType message, TKey messageId = default(TKey), string label = null)
        {
            var m = new BrokeredMessage(message)
            {
                MessageId = messageId!=null && !messageId.Equals(default(TKey)) ? messageId.ToString() : Guid.NewGuid().ToString(),
                Label = label,
                TimeToLive = TimeSpan.FromMinutes(10)
            };

            await _topicClient.SendAsync(m);

            return new AzureBrokeredMessage<TKey, TType>(m, _keyParser);
        }

        public string TopicName { get; private set; }

        public void Dispose()
        {
            if (_topicClient != null)
            {
                _topicClient.Close();
            }
        }
    }
}
