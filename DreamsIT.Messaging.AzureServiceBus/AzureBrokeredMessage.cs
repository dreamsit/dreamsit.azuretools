﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Messaging.Definitions;
using Microsoft.ServiceBus.Messaging;

namespace DreamsIT.Messaging.AzureServiceBus
{
    public class AzureBrokeredMessage<TKey, TType> : IBrokeredMessage<TKey, TType> where TType : class
    {
        private readonly BrokeredMessage _baseMessage;
        private readonly IStringParser<TKey> _keyParser;

        public AzureBrokeredMessage(BrokeredMessage baseMessage, IStringParser<TKey> keyParser)
        {
            _baseMessage = baseMessage;
            _keyParser = keyParser;
        }

        public TType GetContent()
        {
            return _baseMessage.GetBody<TType>();
        }

        public TKey Key
        {
            get { return _keyParser.Parse( _baseMessage.MessageId); }
            set { _baseMessage.MessageId = value.ToString(); }
        }

        public long SequenceNumber
        {
            get { return _baseMessage.SequenceNumber; }
        }

        public string Label
        {
            get { return _baseMessage.Label; }
            set { _baseMessage.Label = value; }
        }
    }
}
