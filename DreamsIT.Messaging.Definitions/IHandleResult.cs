﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Messaging.Definitions
{
    public interface IHandleResult
    {
        bool IsHandled { get; }
        Exception HandleError { get; }
    }
}
