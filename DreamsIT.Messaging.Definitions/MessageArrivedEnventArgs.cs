﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Messaging.Definitions
{
    public class MessageArrivedEnventArgs<TKey,TType>:EventArgs
        where TType:class
    {
        public IBrokeredMessage<TKey,TType> Message { get; set; }
    }
}
