﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Messaging.Definitions
{
    public interface IMessagesSender<TKey,TType>:IDisposable
        where TType:class
    {
        IBrokeredMessage<TKey,TType> SendMessage(TType message, TKey messageId=default(TKey), string label=null);
        Task<IBrokeredMessage<TKey, TType>> SendMessageAsync(TType message, TKey messageId = default(TKey), string label = null);
    }
}
