﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Messaging.Definitions
{
    public interface ITopicSender<TKey, TType> where TType : class
    {
        IBrokeredMessage<TKey,TType> SendMessageToTopic(TType message, TKey messageId = default(TKey), string label = null);
        Task<IBrokeredMessage<TKey, TType>> SendMessageToTopicAsync(TType message, TKey messageId = default(TKey), string label = null);
        string TopicName { get; }
    }
}
