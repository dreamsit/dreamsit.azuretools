﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Messaging.Definitions
{
    public interface IBrokeredMessage<TKey,TType>
        where TType:class
    {
        TType GetContent();
        TKey Key { get; set; }
        long SequenceNumber { get; }
        string Label { get; set; }
    }
}
