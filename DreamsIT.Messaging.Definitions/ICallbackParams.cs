﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Messaging.Definitions
{
    public interface ICallbackParams
    {
        List<Action> SuccessCallbacks { get; set; }
        List<Action> ErrorCallbacks { get; set; }
        List<Action> CompleteCallbacks { get; set; } 
    }

    public interface ICallbackParams<T>
    {
        List<Action<T>> SuccessCallbacks { get; set; }
        List<Action<T>> ErrorCallbacks { get; set; }
        List<Action<T>> CompleteCallbacks { get; set; } 
    }
}
