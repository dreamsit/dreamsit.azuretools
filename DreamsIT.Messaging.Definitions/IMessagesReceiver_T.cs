﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Messaging.Definitions
{
    public interface IMessagesReceiver<TKey,TType>:IDisposable
        where TType:class
    {
        IBrokeredMessage<TKey, TType> Receive(TimeSpan timeout);
        Task<IBrokeredMessage<TKey, TType>> ReceiveAsync(TimeSpan timeout);
        void OnMessage(Action<IBrokeredMessage<TKey, TType>> callback);
    }
}
