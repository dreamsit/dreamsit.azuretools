﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Messaging.Definitions
{
    public interface IMessageHandler<TKey,TType>:IDisposable
        where TType:class
    {
        IHandleResult Handle(IBrokeredMessage<TKey, TType> message, ICallbackParams<IHandleResult> callbacks=null);
        Task<IHandleResult> HandleAsync(IBrokeredMessage<TKey, TType> message, ICallbackParams<IHandleResult> callbacks = null);
    }
}
