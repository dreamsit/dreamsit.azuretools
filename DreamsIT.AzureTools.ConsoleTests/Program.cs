﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Messaging.AzureServiceBus;
using DreamsIT.Messaging.Definitions;

namespace DreamsIT.AzureTools.ConsoleTests
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString =
                @"Endpoint=sb://sftest01.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=BcLxdg1AyjxtoUX6J0h0OjLQPkH9yAEI46qvv1Mv7gY=";

            var sender = new AzureMessageSender<string, MyMessage>(connectionString, "TestQueue", new StringParser());
            var receiver = new AzureMessagesReceiver<string, MyMessage>(connectionString, new StringParser(),
                "TestQueue");

            receiver.OnMessage((message) => Console.WriteLine("Message received:"+message.GetContent().Text));

            var text = "";
            while (text!="q")
            {
                if (!String.IsNullOrEmpty(text))
                {
                    sender.SendMessage(new MyMessage() {Text = text});
                    Console.WriteLine("Message sended.");
                }
                Console.WriteLine("Enter message (or q to exit):");
                text = Console.ReadLine();
            }
        }
    }

    public class MyMessage
    {
        public string Text { get; set; }
    }
}
